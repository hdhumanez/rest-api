CREATE TABLE employee (
    id SERIAL PRIMARY KEY,
    admission_date DATE DEFAULT CURRENT_DATE NOT NULL,
    name VARCHAR(50) NOT NULL,
    salary INT NOT NULL
);

CREATE TABLE request (
    id SERIAL PRIMARY KEY,
    code VARCHAR(50) UNIQUE NOT NULL,
    description VARCHAR(50) NOT NULL,
    resume VARCHAR(50) NOT NULL,
    employee_id INT NOT NULL,
    CONSTRAINT fk_employee FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE RESTRICT
);

CREATE TABLE admin (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) UNIQUE NOT NULL,
    password VARCHAR(200) NOT NULL
);