import { z } from 'zod';
import { IAdmin } from '../interfaces/admin.interface';

const adminSchema = z.object({
  name: z.string().min(1).max(50),
  email: z.string().email(),
  password: z.string().min(6),
});

const loginSchema = z.object({
  email: z.string().email(),
  password: z.string().min(6),
});

const validateAdmin = (data: IAdmin) => {
  return adminSchema.safeParse(data);
};

const validateLogin = (data: { email: string; password: string }) => {
  return loginSchema.safeParse(data);
};

export { validateAdmin, validateLogin };
