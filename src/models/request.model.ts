import { z } from 'zod';
import { IRequest } from '../interfaces/request.interface';

const requestSchema = z.object({
  code: z.string().min(1).max(50).optional(),
  description: z.string().min(1).max(50),
  resume: z.string().min(1).max(50),
  employee_id: z.number().int().positive(),
});

const validateRequest = (data: IRequest) => {
  return requestSchema.safeParse(data);
};

export { validateRequest };
