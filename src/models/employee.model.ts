import { z } from 'zod';
import { IEmployee } from '../interfaces/employee.interface';

const employeeSchema = z.object({
  admission_date: z.string().date(),
  name: z.string().min(1).max(50),
  salary: z.number().int(),
});

const validateEmployee = (data: IEmployee) => {
  return employeeSchema.safeParse(data);
};

export { validateEmployee };
