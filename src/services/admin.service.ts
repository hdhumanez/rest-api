import { IAdmin } from '../interfaces/admin.interface';
import { validateAdmin, validateLogin } from '../models/admin.model';
import { encrypt, verified } from '../utils/bcrypt.handle';
import { generateToken } from '../utils/jwt.handle';
import { ERROR_HANDLE } from '../utils/constants/error.handle';
import { ADMIN_QUERIES } from '../utils/constants/queries';
import { pool } from '../configs/postgre';

const {
  USER_ALREADY_EXISTS,
  INVALID_PAYLOAD,
  USER_NOT_FOUND,
  INCORRECT_PASSWORD,
} = ERROR_HANDLE;

const { INSERT_ADMIN, SELECT_ADMIN_BY_EMAIL } = ADMIN_QUERIES;

const registerNewAdmin = async (newAdmin: IAdmin) => {
  const validatePayload = validateAdmin(newAdmin);
  if (!validatePayload.success) {
    return {
      error: INVALID_PAYLOAD.KEY,
      data: validatePayload.error.message,
    };
  }

  const checkIs = await pool.query(SELECT_ADMIN_BY_EMAIL, [newAdmin.email]);

  if (checkIs.rows.length > 0) {
    return {
      error: USER_ALREADY_EXISTS.KEY,
    };
  }
  const passwordHash = await encrypt(newAdmin.password);
  const values = [newAdmin.name, newAdmin.email, passwordHash];
  const createAdmin = await pool.query(INSERT_ADMIN, values);

  return {
    data: createAdmin,
  };
};

const loginAdmin = async ({ email, password }: IAdmin) => {
  // First we validate the payload to avoid making unnecessary queries to the database
  const validatePayload = validateLogin({ email, password });
  if (!validatePayload.success) {
    return {
      error: INCORRECT_PASSWORD.KEY,
    };
  }
  const checkIs = await pool.query(SELECT_ADMIN_BY_EMAIL, [email]);

  if (checkIs.rows.length === 0) {
    return {
      error: USER_NOT_FOUND.KEY,
    };
  }

  const passwordHash = checkIs.rows[0].password;
  const isCorrect = await verified(password, passwordHash);

  if (!isCorrect) {
    return {
      error: INCORRECT_PASSWORD.KEY,
    };
  }

  const token = generateToken(checkIs.rows[0].id);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { password: _, ...adminWithoutPassword } = checkIs.rows[0];

  return { token, admin: adminWithoutPassword };
};

export { registerNewAdmin, loginAdmin };
