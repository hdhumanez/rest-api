import { IEmployee } from '../interfaces/employee.interface';
import { EMPLOYEE_QUERIES } from '../utils/constants/queries';
import { ERROR_HANDLE } from '../utils/constants/error.handle';
import { pool } from '../configs/postgre';
import { validateEmployee } from '../models/employee.model';

const {
  INSERT_EMPLOYEE,
  DELETE_EMPLOYEE,
  SELECT_EMPLOYEES,
  SELECT_EMPLOYEE_BY_ID,
  UPDATE_EMPLOYEE,
} = EMPLOYEE_QUERIES;

const { INVALID_PAYLOAD } = ERROR_HANDLE;

const createEmployee = async (data: IEmployee) => {
  const validatePayload = validateEmployee(data);
  console.log('FEHCA', validatePayload.data?.admission_date);
  if (!validatePayload.success) {
    return {
      error: INVALID_PAYLOAD.KEY,
      data: validatePayload.error.message,
    };
  }

  const values = [validatePayload.data.admission_date, data.name, data.salary];
  const res = await pool.query(INSERT_EMPLOYEE, values);

  return {
    data: res,
  };
};

const getEmployees = async () => {
  const res = await pool.query(SELECT_EMPLOYEES);
  return res.rows;
};

const getEmployeeById = async (id: string) => {
  const res = await pool.query(SELECT_EMPLOYEE_BY_ID, [id]);
  return res.rows[0];
};

const updateEmployee = async (id: string, data: IEmployee) => {
  const validatePayload = validateEmployee(data);
  if (!validatePayload.success) {
    return {
      error: INVALID_PAYLOAD.KEY,
      data: validatePayload.error.message,
    };
  }

  const values = [
    validatePayload.data.admission_date,
    data.name,
    data.salary,
    id,
  ];

  const res = await pool.query(UPDATE_EMPLOYEE, values);
  return {
    data: res,
  };
};

const deleteEmployee = async (id: string) => {
  const res = await pool.query(DELETE_EMPLOYEE, [id]);
  return res;
};

export {
  createEmployee,
  getEmployees,
  getEmployeeById,
  updateEmployee,
  deleteEmployee,
};
