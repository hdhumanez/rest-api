import { IRequest } from '../interfaces/request.interface';
import { REQUEST_QUERIES } from '../utils/constants/queries';
import { pool } from '../configs/postgre';
import { validateRequest } from '../models/request.model';
import { ERROR_HANDLE } from '../utils/constants/error.handle';

const {
  INSERT_REQUEST,
  SELECT_REQUESTS,
  SELECT_REQUEST_BY_ID,
  UPDATE_REQUEST,
  DELETE_REQUEST,
} = REQUEST_QUERIES;

const { INVALID_PAYLOAD } = ERROR_HANDLE;

const createRequest = async (data: IRequest) => {
  const validatePayload = validateRequest({
    ...data,
    code: `KONECTA-${parseInt(String(Date.now() / 100), 10)}`, // Actual date in milliseconds / 100
  });

  if (validatePayload.error) {
    return {
      error: INVALID_PAYLOAD.KEY,
      data: validatePayload.error.message,
    };
  }

  const values = [
    validatePayload.data.code,
    data.description,
    data.resume,
    data.employee_id,
  ];
  const res = await pool.query(INSERT_REQUEST, values);
  return {
    data: res,
  };
};

const getRequests = async () => {
  const res = await pool.query(SELECT_REQUESTS);
  return res.rows;
};

const getRequestById = async (id: string) => {
  const res = await pool.query(SELECT_REQUEST_BY_ID, [id]);
  return res.rows[0];
};

const updateRequest = async (id: string, data: IRequest) => {
  const validatePayload = validateRequest(data);

  if (validatePayload.error) {
    return {
      error: INVALID_PAYLOAD.KEY,
      data: validatePayload.error.message,
    };
  }

  const values = [data.description, data.resume, data.employee_id, id];
  const res = await pool.query(UPDATE_REQUEST, values);
  return {
    data: res,
  };
};

const deleteRequest = async (id: string) => {
  const res = await pool.query(DELETE_REQUEST, [id]);
  return res;
};

export {
  createRequest,
  getRequests,
  getRequestById,
  updateRequest,
  deleteRequest,
};
