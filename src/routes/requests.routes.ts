import { Router } from 'express';
import {
  createRequestCtrl,
  deleteRequestCtrl,
  getRequestByIdCtrl,
  getRequestsCtrl,
  updateRequestCtrl,
} from '../controllers/request.controller';
import { checkJwt } from '../middlewares/session';
const router = Router();

router.get('/', checkJwt, getRequestsCtrl);
router.get('/:id', checkJwt, getRequestByIdCtrl);
router.post('/', checkJwt, createRequestCtrl);
router.put('/:id', checkJwt, updateRequestCtrl);
router.delete('/:id', checkJwt, deleteRequestCtrl);

export { router };
