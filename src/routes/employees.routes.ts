import { Router } from 'express';
import {
  createEmployeeCtrl,
  deleteEmployeeCtrl,
  getEmployeeByIdCtrl,
  getEmployeesCtrl,
  updateEmployeeCtrl,
} from '../controllers/employee.controller';
import { checkJwt } from '../middlewares/session';
const router = Router();

router.get('/', checkJwt, getEmployeesCtrl);
router.get('/:id', checkJwt, getEmployeeByIdCtrl);
router.post('/', checkJwt, createEmployeeCtrl);
router.put('/:id', checkJwt, updateEmployeeCtrl);
router.delete('/:id', checkJwt, deleteEmployeeCtrl);

export { router };
