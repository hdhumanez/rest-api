import {
  createRequest,
  deleteRequest,
  getRequestById,
  getRequests,
  updateRequest,
} from '../services/request.service';
import { Response } from 'express';
import { INTERNAL_ERROR } from '../utils/constants/error.handle';
import { response } from '../services/response.service';
import { handleHttp } from '../utils/error.handle';
import { RequestExt } from '../interfaces/http.interface';

const getRequestsCtrl = async (req: RequestExt, res: Response) => {
  try {
    const requests = await getRequests();
    return response(res, requests);
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

const getRequestByIdCtrl = async (req: RequestExt, res: Response) => {
  try {
    const { id } = req.params;
    const request = await getRequestById(id);
    return response(res, request);
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

const createRequestCtrl = async (req: RequestExt, res: Response) => {
  try {
    const { body } = req;
    const request = await createRequest(body);

    if (request.error) {
      return handleHttp(res, request);
    }

    return response(res, request.data, 'Request created successfully', 201);
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

const updateRequestCtrl = async (req: RequestExt, res: Response) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const request = await updateRequest(id, body);

    if (request.error) {
      return handleHttp(res, request);
    }

    return response(res, request.data, 'Request updated successfully');
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

const deleteRequestCtrl = async (req: RequestExt, res: Response) => {
  try {
    const { id } = req.params;
    const request = await deleteRequest(id);
    return response(res, request);
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

export {
  getRequestsCtrl,
  getRequestByIdCtrl,
  createRequestCtrl,
  updateRequestCtrl,
  deleteRequestCtrl,
};
