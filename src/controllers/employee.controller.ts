import {
  createEmployee,
  deleteEmployee,
  getEmployeeById,
  getEmployees,
  updateEmployee,
} from '../services/employee.service';
import { Response } from 'express';
import { INTERNAL_ERROR } from '../utils/constants/error.handle';
import { response } from '../services/response.service';
import { handleHttp } from '../utils/error.handle';
import { RequestExt } from '../interfaces/http.interface';

const getEmployeesCtrl = async (req: RequestExt, res: Response) => {
  try {
    const employees = await getEmployees();
    return response(res, employees);
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

const getEmployeeByIdCtrl = async (req: RequestExt, res: Response) => {
  try {
    const { id } = req.params;
    const employee = await getEmployeeById(id);
    return response(res, employee);
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

const createEmployeeCtrl = async (req: RequestExt, res: Response) => {
  try {
    const { body } = req;
    const employee = await createEmployee(body);

    if (employee.error) {
      return handleHttp(res, employee);
    }
    return response(res, employee.data, 'Employee created successfully', 201);
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

const updateEmployeeCtrl = async (req: RequestExt, res: Response) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const employee = await updateEmployee(id, body);

    if (employee.error) {
      return handleHttp(res, employee);
    }

    return response(res, employee.data, 'Employee updated successfully');
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

const deleteEmployeeCtrl = async (req: RequestExt, res: Response) => {
  try {
    const { id } = req.params;
    const employee = await deleteEmployee(id);
    return response(res, employee);
  } catch (error) {
    console.error(error);
    handleHttp(res, INTERNAL_ERROR);
  }
};

export {
  getEmployeesCtrl,
  getEmployeeByIdCtrl,
  createEmployeeCtrl,
  updateEmployeeCtrl,
  deleteEmployeeCtrl,
};
