const EMPLOYEE_QUERIES = {
  INSERT_EMPLOYEE:
    'INSERT INTO employee(admission_date, name, salary) VALUES ($1, $2, $3)',
  SELECT_EMPLOYEES: 'SELECT * FROM employee',
  SELECT_EMPLOYEE_BY_ID: 'SELECT * FROM employee WHERE id = $1',
  UPDATE_EMPLOYEE:
    'UPDATE employee SET admission_date = $1, name = $2, salary = $3 WHERE id = $4',
  DELETE_EMPLOYEE: 'DELETE FROM employee WHERE id = $1',
};

const REQUEST_QUERIES = {
  INSERT_REQUEST:
    'INSERT INTO request(code, description, resume, employee_id) VALUES ($1, $2, $3, $4)',
  SELECT_REQUESTS:
    'SELECT r.id AS request_id, r.code AS request_code, r.description AS request_description, r.resume AS request_resume, e.name AS employee_name, e.id AS employee_id FROM request r JOIN employee e ON r.employee_id = e.id',
  SELECT_REQUEST_BY_ID: 'SELECT * FROM request WHERE id = $1',
  UPDATE_REQUEST:
    'UPDATE request SET description = $1, resume = $2, employee_id = $3 WHERE id = $4',
  DELETE_REQUEST: 'DELETE FROM request WHERE id = $1',
};

const ADMIN_QUERIES = {
  INSERT_ADMIN: 'INSERT INTO admin(name, email, password) VALUES ($1, $2, $3)',
  SELECT_ADMINS: 'SELECT * FROM admin',
  SELECT_ADMIN_BY_ID: 'SELECT * FROM admin WHERE id = $1',
  SELECT_ADMIN_BY_EMAIL: 'SELECT * FROM admin WHERE email = $1',
  UPDATE_ADMIN:
    'UPDATE admin SET name = $1, email = $2, password = $3 WHERE id = $4',
  DELETE_ADMIN: 'DELETE FROM admin WHERE id = $1',
};
export { EMPLOYEE_QUERIES, REQUEST_QUERIES, ADMIN_QUERIES };
