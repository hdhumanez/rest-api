export interface IEmployee {
  id?: string;
  admission_date: Date;
  name: string;
  salary: number;
}
