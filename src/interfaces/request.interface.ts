export interface IRequest {
  id?: string;
  code: string;
  description: string;
  resume: string;
  employee_id: string;
}
