import pg from 'pg';
const { Pool } = pg;

const config: pg.PoolConfig = {
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DATABASE,
};

export const pool = new Pool(config);
